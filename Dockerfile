FROM python:3.6-alpine3.10 as base

ENV PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=random \
    PYTHONUNBUFFERED=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_VERSION=1.0.10

WORKDIR /app/

FROM base as builder
RUN apk --update --no-cache add build-base libffi-dev openssl-dev \
    && pip install "poetry==$POETRY_VERSION" \
    && poetry config virtualenvs.create false
COPY /app/pyproject.toml ./
RUN poetry install --no-interaction --no-dev --no-ansi
#
#FROM base
#COPY --from=builder 
COPY /app/main.py ./
CMD ["uvicorn", "--host", "0.0.0.0", "--port", "8080", "main:app"]
EXPOSE 8080